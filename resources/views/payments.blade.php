
<form method="POST" action="{{ route('paysera-redirect') }}">

    {{ csrf_field() }}

    <label for="email">El.pašto adresas</label>
    <input id="email" type="email" name="email" value="{{ Auth::user()->email }}" >

    <button type="submit" class="btn btn-danger btn-block">Apmokėti</button>

</form>