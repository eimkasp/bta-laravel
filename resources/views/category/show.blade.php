

<h1>{{ $category-> name }}</h1>

@foreach($category->news as $new) 
	<h3>
		<a href="{{ route('news.show', $new->id)}}">
			{{ $new->title }}
			{{ $new->comments->count() }}
		</a>
	</h3>

@endforeach