<ul>
	@foreach($categories as $cat)
		<li>
			<a href="{{ route('category.show', $cat->id) }}">
				{{ $cat->name }}
			</a>
		</li>	
	@endforeach
</ul>