@extends("layouts.blog")


@section('content')
<div class="col-lg-8 col-md-10 mx-auto">
    <h1>@lang('Naujienos')</h1>
    @foreach($news as $newsItem)
          <div class="post-preview">
            <a href="{{ route('news.show', $newsItem->id) }}">
              <h2 class="post-title">
                {{ $newsItem->title }}
              </h2>
              <h3 class="post-subtitle">
                {{ $newsItem->description }}
              </h3>
            </a>
            <p class="post-meta">{{ __('Autorius') }}
              <a href="#">Start Bootstrap</a>
              on {{ $newsItem->created_at->format('Y F d') }}</p>
          </div>
          <hr>
    @endforeach
@endsection
