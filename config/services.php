<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'paysera' => [
         'projectid' => '113619',
         'password' => 'fb3ca24280670b6cde3583783aefeac4',
         'accepturl' => 'http://test.laravel/public/paysera/uzsakymas-pavyko',
         'cancelurl' => 'http://127.0.0.1:8000/paysera/uzsakymas-nepavyko',
         'callbackurl' => 'http://test.laravel/public/paysera/callback',
         'version' => '1.6',
         'test' => 1,
    ],

];
