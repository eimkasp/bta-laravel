<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //

    protected $table = "categories";

    public function news()
    {
        return $this->belongsToMany('App\NewsItem', 'news_categories', 'category_id', 'news_id');
    }
}
