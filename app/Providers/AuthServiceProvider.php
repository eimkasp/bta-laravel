<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Gate::define('edit_news', function ($user) {
           //Be user kintamojo, taip pat gali būti paduoti ir kiti papildomi parametrai
           //Taisyklės patikrinimo kodas
           //Metodas turi gražinti true arba false.
            return $user->admin == 2;
        });
        //
    }
}
