<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\User;

class MenuProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->bind('menu', function ($app) {
            return User::all();
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
